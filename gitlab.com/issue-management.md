# Gitlab issue management

## Issue weight

[Adblock Plus's Trac Issue Priority System](https://issues.adblockplus.org/wiki/TracGuide#Priority) was adapted from [Mozilla's Bugzilla Priority System](https://wiki.mozilla.org/Bugzilla:Priority_System).

Although Gitlab's weight system uses 1-9 instead of P1-P5, we maintain philosophical backwards compatibility by converting P1-P5 into weights 1-5.

1. This is time-critical, and needs to get tackled immediately.
2. This should get done as soon as possible.
3. We definitely want this, but it's not a priority.
4. We wouldn't mind having this, but it's unlikely to get done by the core team. Patches are welcome.
5. It's not clear if we really want to have this, needs to be discussed first.