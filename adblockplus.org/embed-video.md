# Embeding YouTube video into textpattern

The following snippet of code may be used to embed YouTube video into textpattern posts.

```html
<div class="video-parent">
  <a
    class="video-link"
    target="_blank"
    href="YOUTUBE_URL">
    <img
      class="video-thumbnail"
      src="THUMBNAIL_IMAGE"
      alt="THUMBNAIL_ALT_TEXT">
    <img
      class="video-play"
      src="/img/video-external.png"
      alt="Play Video">
  </a>
  <p class="video-disclaimer">
    Click to play this video. Please note that this video is hosted by YouTube. When this video is played, some personal data is transferred to YouTube. For more information, please review YouTube's <a href='https://www.google.com/intl/en/policies/privacy/' target='_blank'>privacy policy</a>.
  </p>
</div>
```

Please replace the following in the snippet above before publishing.

## YOUTUBE_URL

1. Goto video on YouTube
2. Click "Share" below video
3. Click "Embed" inside "Share" menu
4. Copy the embed ID between `/embed/` and `"` in the embed code provided
  - **Example**: `abcDEF12345` is the embed code from `<iframe width="560" height="315" src="https://www.youtube.com/embed/abcDEF12345" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>`
5. Paste the embed ID over `EMBED_ID_HERE` in  `https://www.youtube-nocookie.com/embed/EMBED_ID_HERE?html5=1&amp;autohide=1&amp;enablejsapi=1&amp;controls=2&amp;fs=1&amp;modestbranding=1&amp;rel=0&amp;showinfo=0&amp;theme=light&amp;autoplay=1` into `YOUTUBE_URL` in the snippet above

## THUMBNAIL_IMAGE

1. Goto the "Images" tab in textpattern
2. Upload an image
3. Copy the "view" link next to the upload image
4. Paste the link into `THUMBNAIL_IMAGE` in the snippet above

## THUMBNAIL_ALT_TEXT

Replace `THUMBNAIL_ALT_TEXT` in the snippet above with a text description of the thumbnail image above. 

(See [w3's accessible image concepts tutorial](https://www.w3.org/WAI/tutorials/images/) for guidance.)
