# Legal guidelines for eyeo Help Center

This document outlines when an article needs legal review/approval prior to publication.

## What types of articles require legal review/approval?

- If the primary focus of the article is Acceptable Ads or the Acceptable Ads Committee
- If the primary focus of the article is eyeo’s financials and/or user numbers 
- If the primary focus of the article is personal data/data collection
- If the article describes how to deal with anti-ad block techniques, especially “lock-out” screens or ad blocking-user targeted messages including how to avoid legally required information like “cookie notifications”
- If the article describes how something works as opposed to steps on how to do something, e.g. How does ad blocking work?, What is element hiding?, How do filters work?, etc. If these articles are incorrect, they could potentially support copyright claims that plaintiffs file against eyeo.

## Things to keep in mind

- Articles that link to anything about Acceptable Ads must be linked to AdblockPlus.org/accaptable-ads and/or its relevant subsections because AcceptableAds.com is not translated. Because AcceptableAds.com is not translated, legal issues could arise if a user cannot view the content in German or French.
- If you are unsure if an article needs legal review/approval, or if you want to improve this guideline, notify the Legal team via the [Help Center project](https://gitlab.com/eyeo/websites/help.eyeo.com).
