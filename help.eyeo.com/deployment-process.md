# GitLab based website deployment process

## FAQ

### How deploy a new version of the websites?

Push the new changes into the master branch, this will trigger the pipeline defined in the projects [.gitlab-ci.yml](https://gitlab.com/eyeo/websites/help.eyeo.com/blob/master/.gitlab-ci.yml).

This will build the website and upload a tarball to the corresponding *eyeofiles.com* artifact repository, see [Hub#5080](http://hub.eyeo.com/issues/5080).

Once the artifact has been uploaded it can be deployed by issuing the following command, see [Hub#4847](http://hub.eyeo.com/issues/4847). Replace the hash with the abbreviated commit hash (the first 8 characters) of current HEAD.

```bash
$ ssh helpcenter-deploy@help.eyeo.com -- help.eyeo.com --source=https://helpcenter.eyeofiles.com --name=1234abcde
Downloading: 1234abcdef.tar.gz
Downloading: 1234abcdef.md5
Extracted in current directory
```

Users can only run certain commands in the server, you can see which commands with:

```bash
$ ssh helpcenter-deploy@help.eyeo.com -- help
Avaiable commands are:
help
uptime
help.eyeo.com
```

In the case of the deployment command

```bash
ssh helpcenter-deploy@help.eyeo.com -- help.eyeo.com --source=https://helpcenter.eyeofiles.com --name=1234abcde
```

The command itself is the name of the website to deploy, this is done by ops team
--source flag must be the entire domain name from where the file is gonna be fetched
--name flag is the name of both files (.tar.gz and md5)

### How can I deploy a version has not been merged in the master branch?

You can't, the job that uploads the build artifact to the *eyeofiles.com* repository is only active on the master branch, but the page is build for every commit. You can download the artifact via [gitlab](https://docs.gitlab.com/ce/user/project/pipelines/job_artifacts.html#downloading-artifacts).

In the future we plan to add a staging system where you can preview development versions.

## Overview

The deployment process consists of three parts:

1. [.gitlab-ci.yml](https://gitlab.com/eyeo/websites/help.eyeo.com/blob/master/.gitlab-ci.yml) that builds the website using gulp and uploads it to the *eyeofiles.com* repository.
2. The *eyeofiles.com* artifact repository. A file storage for secure access for the production system
3. The deployment script running on the production webserver that downloads the websites from *eyeofiles.com* and publishes it.

## .gitlab-ci.yml

The  [.gitlab-ci.yml](https://gitlab.com/eyeo/websites/help.eyeo.com/blob/master/.gitlab-ci.yml) configuration defines two jobs:

- [generate-pages](#generate-pages)
- [deploy-pages](#deploy-pages)

### generate-pages

This job builds the websites using our cms and provides the result via the gitlab [artifacst](https://docs.gitlab.com/ce/ci/yaml/README.html#artifacts) interface. This step is configured to run for each commit.

### deploy-pages

The deploy-pages job uploads the [previously](#generate-pages) created artifact to the *eyeofiles.com* service. This job is configured to only run for the *master* branch.

## eyeofiles.com

The *eyeofiles.com* file service (see [hub#2317](http://hub.eyeo.com/issues/2317)) is used to store the artifacts for the production system to download.

## Deployment script

A deployment is triggered by running the deployment ssh command on the production webserver. It takes the abbreviated commit hash (`git log --format=%h`) of the version that should be deployed. The version has to be present on the [eyeofiles.com](#eyeofiles-com) service.

```bash
$ ssh helpcenter-deploy@help.eyeo.com -- help.eyeo.com --source=https://helpcenter.eyeofiles.com --name=1234abcde
Downloading: 1234abcdef.tar.gz
Downloading: 1234abcdef.md5
Extracted in current directory
```

## Should I tag each deployment?

Each version that is deployed should be tagged with a simple date based schema (YYYYMMDD), see [here](https://gitlab.com/eyeo/websites/help.eyeo.com/tags)
