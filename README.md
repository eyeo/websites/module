# Websites Module

eyeo websites module documentation.

## Contents

- module/
  - [Contributors](CONTRIBUTORS.md)
- gitlab/
  - [Board management](gitlab.com/gitlab-board.md)
  - [Issue management](gitlab.com/issue-management.md)
- help.eyeo.com/
  - [Deployment process](help.eyeo.com/deployment-process.md)
  - [Legal guidelines](help.eyeo.com/legal-guidelines.md)
- acceptableads.com/
  - [Sync with gitlab](acceptableads.com/sync-with-gitlab.md)
- adblockplus.org/
  - [Embed video](adblockplus.org/embed-video.md)
