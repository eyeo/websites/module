# How to sync git repository with a mercurial repository

Prerequisite:

1. Install [hg-fast-export](https://github.com/frej/fast-export)
2. Update local hg clone `https://hg.adblockplus.org/web.acceptableads.com/`
3. Update local git clone `git@gitlab.com:eyeo/websites/web.acceptableads.com.git`

```
cd PATH_TO_GIT_REPO
hg-fast-export -r PATH_TO_HG_REPO
```

If done once successfully the repositories can be synced by simply issuing `hg-fast-export` from inside the git repository path. 

```
$ cat .git/hg2git-state 
:repo ../../hg/web.acceptableads.com/
:tip 271
```